<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

//
// spip-core
//
'accueil_site' => 'Accueil',

// 0-9
'404_sorry' => 'Désolé, cette page n\'existe plus.',

// A
'articles_tous' => 'Tous les articles',
'article_recherche_titre' => 'Résultats sur les articles',
'adresse' => 'Adresse',
'activites_titre' => 'Nos activités',
'article_recherche_nb_1' => '1 article disponible',
'article_recherche_nbs' => '@nb@ articles disponibles',


// B
'but_savoir_plus' => 'En savoir plus',

// C
'cacher_a2a' => 'Ne pas afficher de suggestions en bas d\'article',
'cacher_date' => 'Ne pas afficher la date de cet article',
'cfg_color_white' => 'Blanc',
'cfg_color_black' => 'Noir',
'cfg_bg_color' => 'Couleur du fond de page',
'cfg_color_primary_color' => 'Couleur principale',
'cfg_color_primary_color_hover' => 'Couleur principale (au&nbsp;survol)',
'cfg_color_primary_color_light' => 'Couleur principale (éclaircie)',
'cfg_color_secondary_color' => 'Couleur secondaire',
'cfg_color_secondary_color_hover' => 'Couleur secondaire (au&nbsp;survol)',
'cfg_color_secondary_color_light' => 'Couleur secondaire (éclaircie)',
'cfg_color_secondary_color_super_light' => 'Couleur secondaire (très éclaircie)',
'cfg_color_ternary_color' => 'Couleur tertiaire',
'cfg_color_gray' => 'Gris',
'cfg_color_gray_light' => 'Gris (clair)',
'cfg_color_gray_dark' => 'Gris (sombre)',
'cfg_is_shadow' => 'Activer l\'ombre sur les boutons',
'cfg_contact' => 'Contact',
'cfg_pages' => 'Pages spéciales',
'cfg_titre_parametrages' => 'Configurer Ginza',
'cfg_homepage' => 'Page d\'accueil',
'cfg_home_reassurance' => 'Article de présentation (réassurance)',
'cfg_home_activites' => 'Rubrique Activités',
'cfg_footer' => 'Pied de page',
'cfg_footer_copyright_articles1' => 'Liens - colonne 1',
'cfg_footer_copyright_articles1_explication' => 'Liens recommandés en pied de page (crédits, mentions légales ...). Dans un contexte multilingue, pensez à ajouter les liens dans chaque langue',
'cfg_footer_copyright_articles2' => 'Liens - colonne 2',
'cfg_footer_copyright_articles2_explication' => 'Liens recommandés en pied de page (crédits, mentions légales ...). Dans un contexte multilingue, pensez à ajouter les liens dans chaque langue',
'cfg_footer_partenaires' => 'Liens - Partenaires',
'cfg_footer_partenaires_explication' => 'Article dont le texte contient les liens vers les partenaires avec leurs logos',
'cfg_rezo' => 'Réseaux sociaux',
'cfg_rezo_facebook' => 'Facebook',
'cfg_rezo_twitter' => 'Twitter',
'cfg_rezo_linkedin' => 'LinkedIn',
'cfg_rezo_youtube' => 'Youtube',
'cfg_rezo_instagram' => 'Instagram',
'cfg_menu' => 'Bannière et menu',
'cfg_menu_lang' => 'Ajouter le menu dans langues dans le bandeau',
'cfg_menu_ids' => 'Menu principal',
'cfg_menu_ids_explication' => 'Rubriques et articles qui constituent le menu. Limiter le nombre de liens à 6 maximum. Dans un site multilingue, pensez à ajouter les items dans chacun des langues.',
'cfg_intro' => 'Cette page vous permet de personnaliser votre site',
'cfg_lien_doc' => 'Documentation en ligne',
'cfg_liens' => 'Liens importants',
'cfg_palette' => 'Palette',
'cfg_palette_explication' => 'Modifier le jeu des couleurs utilisées par ginza',
'cfg_palette_explication_suite' => 'Après avoir enregistré vos couleurs (en appuyant sur le bouton enregistrer en bas de ce formulaire), pensez à vider votre cache pour mettre à jour la feuille de style du site publique',
'cfg_page_demo' => 'Le squelette est livré avec une page démonstration qui permet de tester la mise en page avec des contenus factices :',
'cfg_page_demo_article' => 'Démo article',
'cfg_bouton1_article' => 'Article du bouton 1',
'cfg_bouton2_article' => 'Article du bouton 2',
'cfg homepage intro' => 'Article du bouton 2',
'cfg_home_intro_titre' => 'Titre',
'cfg_home_intro_soustitre' => 'Sous-titre',
'cfg_home_intro_texte' => 'Texte',
'contact' => 'Contact',
'cfg_homepage_intro' => 'Page d\'accueil > bloc introduction',
'cfg_pagination_titre' => 'Pagination',
'cfg_pagination' => 'Nombre d\'items listés par page de pagination',
'cfg_pagination_explication' => 'Lorsque qu\'il y a beaucoup de résultats, le site utilise un système de pagination. Indiquer le nombre d\'items listés par page. Si vous ne souhaitez pas de pagination, indiquer -1',
'cfg_home_reassurance' => 'Texte  de réassurance',
'cfg_home_reassurance_explication' => 'Texte d\'introduction du site. Dans un contexte multilingue, pensez à utiliser les balises &lt;multi&gt; ... &lt;/multi&gt;',
'cfg_home_reassurance_ids' => 'Lien du bouton',
'cfg_home_reassurance_ids_explication' => '(Facultatif) Choisir le lien vers lequel renvoie le bouton. Dans un contexte multilingue choisir un lien par langue',
'cfg_home_reassurance_bouton_titre' => 'Texte du bouton',
'cfg_home_reassurance_bouton_titre_explication' => 'Dans un contexte multilingue, pensez à utiliser les balises &lt;multi&gt; ... &lt;/multi&gt;',
'cfg_home_une' => 'Page d\'accueil > à la une  (articles affichés en pleine largeur)',
'cfg_home_une_ids' => 'Articles à la une',
'cfg_home_une_ids_explication' => '(Facultatif) Articles à la Une sont listés en plein largeur. Dans un contexte multilingue, pensez à sélectionner des articles dans chaque langue',
'cfg_home_une_selection_editoriale' => 'Articles à la une (sélection éditoriale)',
'cfg_home_une_selection_editoriale_explication' => '(Facultatif) Vous pouvez aussi gérer vos Articles à la Une avec une sélection éditoriale. Dans un contexte multilingue, pensez à sélectionner des articles dans chaque langue',
'cfg_home_recents' => 'Page d\'accueil > Articles récents (articles affichés en demi-largeur)',
'cfg_home_recents_ids' => 'Articles récents (manuel)',
'cfg_home_recents_ids_explication' => '(Facultatif) Articles récents sont listés en demi-largeur. Sélectionner ici les articles à top-lister manuellement (en dehors des contraintes de dates). Dans un contexte multilingue, pensez à sélectionner des articles dans chaque langue',
'cfg_home_recents_selection_editoriale' => 'Articles récents (sélection éditoriale)',
'cfg_home_recents_selection_editoriale_explication' => '(Facultatif) Articles récents sont listés en demi-largeur. Sélectionner ici les articles à top-lister manuellement (en dehors des contraintes de dates). Dans un contexte multilingue, pensez à sélectionner des articles dans chaque langue',
'cfg_home_recents_nb_articles' => 'Nombre d\'articles récents (automatique)',
'cfg_home_recents_nb_articles_explication' => 'Nombre d\'articles récents à lister automatiquement par date. Pour ne rien lister automatiquement indiquer -1',
'cfg_home_aussi' => 'Page d\'accueil > Et aussi (articles affichés sous forme de liste)',
'cfg_home_aussi_ids' => 'Et aussi... (manuel)',
'cfg_home_aussi_ids_explication' => '(Facultatif) Ces articles sont listés sous forme de liste simple. Dans un contexte multilingue, pensez à sélectionner des articles dans chaque langue',
'cfg_home_aussi_selection_editoriale' => 'Et aussi... (sélection éditoriale)',
'cfg_home_aussi_selection_editoriale_explication' => '(Facultatif) Ces articles sont listés sous forme de liste simple. Dans un contexte multilingue, pensez à sélectionner des articles dans chaque langue',
'cfg_home_aussi_nb_articles' => 'Nombre d\'articles "Lire aussi" (automatique)',
'cfg_home_aussi_nb_articles_explication' => 'Nombre d\'articles "Lire aussi" à lister automatiquement par date. Pour ne rien lister automatiquement indiquer -1',



// E
'et_aussi' => 'Voir aussi&nbsp;...',
'en_savoir_plus' => 'En savoir plus',

// F
'forum' => 'Forum',
'forum_derniers' => 'Les derniers sujets du forum',
'forum_acceder' => 'Accéder au forum',
'forums_nb_post' => 'Il y a plus de @nb@ sujets',
'forums_nb_post_0' => 'Il n\'y a aucune réponse à ce message',
'forums_nb_post_1' => '1 réponse disponible',
'forums_nb_post_nb' => ' @nb@ réponses disponibles',
'forums_post_cta' => 'Écrire un nouveau message',
'forum_recherche' => 'Rechercher sur le forum',
'forum_champs_dernier_post' => 'Derniers messages',
'forum_champs_dernier_thread' => 'Derniers sujets',
'forum_champs_auteur' => 'Auteur',
'forum_champs_date' => 'Date',
'forum_poster_nouveau_message' => 'Poster un nouveau message',
'forums_lire_titre' => 'Participer',
'forums_lire_texte' => 'Consulter et répondre aux messages postés',
'forums_lire_cta' => 'Je participe',
'forums_post_titre' => 'Poster un nouveau message',
'forums_post_texte' => 'Lancer une discussion',
'forums_post_reply_cta' => 'Poster une nouvelle réponse',
'forum_post_recent' => 'Nouveau',
'forum_recherche_annuler' => 'Annuler cette recherche',
'forum_recherche_titre' => 'Résultats sur le forum',


// L
'liens' => 'Liens',
'lire_la_suite' => 'Lire la suite',
'lire_la_suite_decouvrir' => 'Découvrir',
'les_evenements' => 'Les événements',


// M
'menu' => 'Menu',
'menu_lang' => 'Langue',
'mis_a_jour' => 'Mis à jour le',



// O
'ours' => 'Le saviez-vous ?<br />Le nom du squelette <strong>Ginza</strong> (銀座) vient d\'un quartier chic de Tōkyō.',


// P
'publie_le' => 'Publié le',
'par' => 'par',
'pagination_pages' => 'Pages',
'pagination_gd_total' => 'articles disponibles',
'pagination_environ' => 'Environ',
'portfolio' => 'Portfolio',
'presentation' => 'Présentation',


// R
'resultats_out' => 'Résultat(s) disponible(s)',
'recherche_site' => 'Résultat sur  : ',
'recherche_recherche' => 'Rechercher',
'recherche_archive' => 'Rechercher',
'recherche_nomatch'  => 'Désolé, <strong>aucun résultat</strong> disponible sur cette recherche !<br>Modifier votre recherche pour élargir les résultats ou utiliser le caractère * comme joker',
'resultats_articles' => 'Recherche sur les articles',
'recherche_dans_rubrique' => 'Rechercher dans cette rubrique',
'recherche_resultat' => 'Résultats de la recherche sur',
'recherche_titre' => 'Rechercher',
'recherche_cancel' => 'annuler cette recherche',
'retour_liste' => 'Retour à la liste',
'resultats' => '&nbsp;résultat(s)',
'repondre_article' => 'Commenter',


// T
'top' => 'Haut de page',
'titre_page_configurer_ginza' => 'Configurer Ginza',

// V
'ginza_type_rubrique' => 'Type de rubrique',
'ginza_type_rubrique_tri_date' => 'Articles listés par date (les articles les récents en premier)',
'ginza_type_rubrique_tri_num' => 'Articles listés par numéro (10. xxx, 20. yyy, ...)',
'ginza_type_rubrique_tri_faq' => 'Articles listés comme une base de connaissances (FAQ)',
'ginza_type_rubrique_tri_evenement' => 'Articles listés comme un agenda (liste d\'événements)',
'ginza_rubrique_surtitre' => 'Surtitre',
'ginza_rubrique_surtitre_explication' => '(Facultatif) Permet d\'afficher une phrase courte au dessus du titre notamment sur le page d\'accueil',
'ginza_rubrique_titre_long' => 'Titre long',
'ginza_rubrique_titre_long_explication' => '(Facultatif) Permet d\'afficher un titre long notamment sur le page d\'accueil',


];

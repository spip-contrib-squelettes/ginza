<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

//
// spip-core
//
'accueil_site' => 'Home',

// 0-9
'404_sorry' => 'Sorry, this page doesn\'t exist anymore.',

// A
'articles_tous' => 'All Articles',
'article_recherche_titre' => 'Results on Articles',
'adresse' => 'Address',
'activites_titre' => 'Our Activities',
'article_recherche_nb_1' => '1 article available',
'article_recherche_nbs' => '@nb@ pages available',

// B
'but_savoir_plus' => 'Learn More',

// C
'cacher_a2a' => 'Do not display suggestions at the bottom of the article',
'cacher_date' => 'Do not display the date of this article',
'cfg_color_white' => 'White',
'cfg_color_black' => 'Black',
'cfg_bg_color' => 'Background Color',
'cfg_color_primary_color' => 'Main Color',
'cfg_color_primary_color_hover' => 'Main Color (on hover)',
'cfg_color_primary_color_light' => 'Main Color (lightened)',
'cfg_color_secondary_color' => 'Secondary Color',
'cfg_color_secondary_color_hover' => 'Secondary Color (on hover)',
'cfg_color_secondary_color_light' => 'Secondary Color (lightened)',
'cfg_color_secondary_color_super_light' => 'Secondary Color (very lightened)',
'cfg_color_ternary_color' => 'Tertiary Color',
'cfg_color_gray' => 'Gray',
'cfg_color_gray_light' => 'Light Gray',
'cfg_color_gray_dark' => 'Dark Gray',
'cfg_is_shadow' => 'Enable shadow on buttons',
'cfg_contact' => 'Contact',
'cfg_pages' => 'Special Pages',
'cfg_titre_parametrages' => 'Configure Ginza',
'cfg_homepage' => 'Homepage',
'cfg_home_reassurance' => 'Introduction Article',
'cfg_home_activites' => 'Activities Section',
'cfg_footer' => 'Footer',
'cfg_footer_copyright_articles1' => 'Links - Column 1',
'cfg_footer_copyright_articles1_explication' => 'Recommended links in the footer (credits, legal notices ...). In a multilingual context, remember to add the links in each language',
'cfg_footer_copyright_articles2' => 'Links - Column 2',
'cfg_footer_copyright_articles2_explication' => 'Recommended links in the footer (credits, legal notices ...). In a multilingual context, remember to add the links in each language',
'cfg_footer_partenaires' => 'Links - Partners',
'cfg_footer_partenaires_explication' => 'Article containing links to partners with their logos',
'cfg_rezo' => 'Social Media',
'cfg_rezo_facebook' => 'Facebook',
'cfg_rezo_twitter' => 'Twitter',
'cfg_rezo_linkedin' => 'LinkedIn',
'cfg_rezo_youtube' => 'Youtube',
'cfg_rezo_instagram' => 'Instagram',
'cfg_menu' => 'Banner and Menu',
'cfg_menu_lang' => 'Add language menu to the banner',
'cfg_menu_ids' => 'Main Menu',
'cfg_menu_ids_explication' => 'Sections and articles that make up the menu. Limit the number of links to a maximum of 6. In a multilingual site, remember to add items in each language.',
'cfg_intro' => 'This page allows you to customize your site',
'cfg_lien_doc' => 'Online Documentation',
'cfg_liens' => 'Important Links',
'cfg_palette' => 'Color Palette',
'cfg_palette_explication' => 'Modify the color scheme used by ginza',
'cfg_palette_explication_suite' => 'After saving your colors (by pressing the save button at the bottom of this form), remember to clear your cache to update the public site\'s style sheet.',
'cfg_page_demo' => 'The template comes with a demo page that allows you to test the layout with dummy content:',
'cfg_page_demo_article' => 'Demo article',
'cfg_bouton1_article' => 'Button 1 Article',
'cfg_bouton2_article' => 'Button 2 Article',
'cfg homepage intro' => 'Button 2 Article',
'cfg_home_intro_titre' => 'Title',
'cfg_home_intro_soustitre' => 'Subtitle',
'cfg_home_intro_texte' => 'Text',
'contact' => 'Contact',
'cfg_homepage_intro' => 'Homepage > Introduction Block',
'cfg_pagination_titre' => 'Pagination',
'cfg_pagination' => 'Number of items listed per pagination page',
'cfg_pagination_explication' => 'When there are many results, the site uses a pagination system. Please specify the number of items listed per page. If you do not want pagination, indicate -1',
'cfg_home_reassurance' => 'Reassurance Text',
'cfg_home_reassurance_explication' => 'Site introduction text. In a multilingual context, consider using the <multi> ... </multi> tags',
'cfg_home_reassurance_ids' => 'Button Link',
'cfg_home_reassurance_ids_explication' => '(Optional) Choose the link the button leads to. In a multilingual context, choose one link per language',
'cfg_home_reassurance_bouton_titre' => 'Button Text',
'cfg_home_reassurance_bouton_titre_explication' => 'In a multilingual context, consider using the <multi> ... </multi> tags.',
'cfg_home_une' => 'Homepage > Featured',
'cfg_home_une_ids' => 'Featured Articles',
'cfg_home_une_ids_explication' => '(Optional) Featured Articles are listed in full width. In a multilingual context, consider selecting articles in each language',
'cfg_home_recents' => 'Homepage > Recent Articles',
'cfg_home_recents_ids' => 'Recent Articles (manual)',
'cfg_home_recents_ids_explication' => '(Optional) Recent articles are listed in half-width. Select here the articles to manually top-list (outside of date constraints). In a multilingual context, consider selecting articles in each language',
'cfg_home_recents_nb_articles' => 'Number of recent articles (automatic)',
'cfg_home_recents_nb_articles_explication' => 'Number of recent articles to list automatically by date. To list nothing automatically, indicate -1',
'cfg_home_aussi' => 'Homepage > And Also',
'cfg_home_aussi_ids' => 'And Also... (manual)',
'cfg_home_aussi_ids_explication' => '(Optional) Select articles to place in "Read also". These articles are listed as a simple list. In a multilingual context, consider selecting articles in each language',
'cfg_home_aussi_nb_articles' => 'Number of "Read also" articles (automatic)',
'cfg_home_aussi_nb_articles_explication' => 'Number of "Read also" articles to list automatically by date. To list nothing automatically, indicate -1',


// E
'et_aussi' => 'See also ...',
'en_savoir_plus' => 'Learn more',

// F
'forum' => 'Forum',
'forum_derniers' => 'The latest topics on the forum',
'forum_acceder' => 'Access the forum',
'forums_nb_post' => 'There are more than @nb@ topics',
'forums_nb_post_0' => 'There are no replies to this message',
'forums_nb_post_1' => '1 response available',
'forums_nb_post_nb' => '@nb@ responses available',
'forums_post_cta' => 'Write a new message',
'forum_recherche' => 'Search on the forum',
'forum_champs_dernier_post' => 'Latest messages',
'forum_champs_dernier_thread' => 'Latest topics',
'forum_champs_auteur' => 'Author',
'forum_champs_date' => 'Date',
'forum_poster_nouveau_message' => 'Post a new message',
'forums_lire_titre' => 'Participate',
'forums_lire_texte' => 'View and reply to posted messages',
'forums_lire_cta' => 'I participate',
'forums_post_titre' => 'Post a new message',
'forums_post_texte' => 'Start a discussion',
'forums_post_reply_cta' => 'Post a new reply',
'forum_post_recent' => 'New',
'forum_recherche_annuler' => 'Cancel this search',
'forum_recherche_titre' => 'Results on the forum',


// L
'liens' => 'Links',
'lire_la_suite' => 'Read more',
'lire_la_suite_decouvrir' => 'Discover',
'les_evenements' => 'Events',


// M
'menu' => 'Menu',
'menu_lang' => 'Language',
'mis_a_jour' => 'Updated on',


// O
'ours' => 'Did you know?<br />The name of the skeleton <strong>Ginza</strong> (銀座) comes from a chic district of Tokyo.',


// P
'publie_le' => 'Published on',
'par' => 'by',
'pagination_pages' => 'Pages',
'pagination_gd_total' => 'available articles',
'pagination_environ' => 'Approximately',
'portfolio' => 'Portfolio',
'presentation' => 'Layout',

// R
'resultats_out' => 'Result(s) available',
'recherche_site' => 'Result on: ',
'recherche_recherche' => 'Search',
'recherche_archive' => 'Search',
'recherche_nomatch'  => 'Sorry, <strong>no results</strong> available for this search! <br>Modify your search to broaden the results or use the * character as a wildcard',
'resultats_articles' => 'Search in articles',
'recherche_dans_rubrique' => 'Search in this section',
'recherche_resultat' => 'Search results for',
'recherche_titre' => 'Search',
'recherche_cancel' => 'Cancel this search',
'retour_liste' => 'Back to list',
'resultats' => '&nbsp;result(s)',
'repondre_article' => 'Comment',


// T
'top' => 'Top of page',
'titre_page_configurer_ginza' => 'Configure ginza',

// V
'ginza_type_rubrique' => 'Section type',
'ginza_type_rubrique_tri_date' => 'Articles listed by date (latest articles first)',
'ginza_type_rubrique_tri_num' => 'Articles listed by number (10. xxx, 20. yyy, ...)',
'ginza_type_rubrique_tri_faq' => 'Articles listed as a knowledge base (FAQ)',
'ginza_type_rubrique_tri_evenement' => 'Articles listed as an agenda (list of events)',
'ginza_rubrique_surtitre' => 'Subheading',
'ginza_rubrique_surtitre_explication' => '(Optional) Allows displaying a short phrase above the title, especially on the homepage',
'ginza_rubrique_titre_long' => 'Long title',
'ginza_rubrique_titre_long_explication' => '(Optional) Allows displaying a long title, especially on the homepage',





];


<?php
// هذا ملف لغة SPIP  --  Ceci est un fichier langue de SPIP

return [

//
// spip-core
//
'accueil_site' => 'الصفحة الرئيسية',

// 0-9
'404_sorry' => 'عذرًا، هذه الصفحة لم تعد موجودة',

// A
'articles_tous' => 'جميع المقالات',
'article_recherche_titre' => 'النتائج على المقالات',
'adresse' => 'العنوان',
'activites_titre' => 'أنشطتنا',
'article_recherche_nb_1' => 'مقالة واحدة متوفرة',
'article_recherche_nbs' => 'مقالة متوفرة @nb@',


// B
'but_savoir_plus' => 'تعرف أكثر',

// C
'cacher_a2a' => 'عدم عرض اقتراحات في أسفل المقالة',
'cacher_date' => 'عدم عرض تاريخ هذه المقالة',
'cfg_color_white' => 'أبيض',
'cfg_color_black' => 'أسود',
'cfg_bg_color' => 'لون خلفية الصفحة',
'cfg_color_primary_color' => 'اللون الرئيسي',
'cfg_color_primary_color_hover' => 'اللون الرئيسي (عند التحويم)',
'cfg_color_primary_color_light' => 'اللون الرئيسي (مُضَيِّء)',
'cfg_color_secondary_color' => 'اللون الثانوي',
'cfg_color_secondary_color_hover' => 'اللون الثانوي (عند التحويم)',
'cfg_color_secondary_color_light' => 'اللون الثانوي (مُضَيِّء)',
'cfg_color_secondary_color_super_light' => 'اللون الثانوي (مُضَيِّء جداً',
'cfg_color_ternary_color' => 'اللون الثالثوي',
'cfg_color_gray' => 'رمادي',
'cfg_color_gray_light' => 'رمادي فاتح',
'cfg_color_gray_dark' => 'رمادي داكن',
'cfg_is_shadow' => 'تفعيل الظل على الأزرار',
'cfg_contact' => 'اتصل',
'cfg_pages' => 'صفحات خاصة',
'cfg_titre_parametrages' => 'ginza تكوين',
'cfg_homepage' => 'الصفحة الرئيسية',
'cfg_home_reassurance' => 'مقالة تقديمية',
'cfg_home_activites' => 'قسم الأنشطة',
'cfg_footer' => 'تذييل الصفحة',
'cfg_footer_copyright_articles1' => 'الروابط - العمود 1',
'cfg_footer_copyright_articles1_explication' => 'الروابط الموصى بها في التذييل (الاعتمادات، الإشعارات القانونية ...). في سياق متعدد اللغات، تذكر أن تضيف الروابط في كل لغة',
'cfg_footer_copyright_articles2' => 'الروابط - العمود 2',
'cfg_footer_copyright_articles2_explication' => 'الروابط الموصى بها في التذييل (الاعتمادات، الإشعارات القانونية ...). في سياق متعدد اللغات، تذكر أن تضيف الروابط في كل لغة',
'cfg_footer_partenaires' => 'الروابط - الشركا',
'cfg_footer_partenaires_explication' => 'مقالة تحتوي على روابط إلى الشركاء مع شعاراتهم',
'cfg_rezo' => 'وسائل التواصل الاجتماعي',
'cfg_rezo_facebook' => 'فيسبوك',
'cfg_rezo_twitter' => 'تويتر',
'cfg_rezo_linkedin' => 'لينكدإن',
'cfg_rezo_youtube' => 'يوتيوب',
'cfg_rezo_instagram' => 'إنستغرام',
'cfg_menu' => 'بانر وقائمة',
'cfg_menu_lang' => 'إضافة قائمة اللغات إلى البانر',
'cfg_menu_ids' => 'القائمة الرئيسية',
'cfg_menu_ids_explication' => 'الأقسام والمقالات التي تشكل القائمة. قم بتقييد عدد الروابط إلى ما يصل إلى 6. في موقع متعدد اللغات، تذكر أن تضيف العناصر في كل لغة',
'cfg_intro' => 'تتيح لك هذه الصفحة تخصيص موقعك',
'cfg_lien_doc' => 'التوثيق عبر الإنترنت',
'cfg_liens' => 'الروابط المهمة',
'cfg_palette' => 'لوحة الألوان',
'cfg_palette_explication' => 'ginza تعديل مجموعة الألوان المستخدمة بواسطة',
'cfg_palette_explication_suite' => 'بعد حفظ ألوانك (بالضغط على زر الحفظ في أسفل هذا النموذج)، تذكر أن تقوم بمسح ذاكرة التخزين المؤقت لتحديث ورقة الأنماط الخاصة بالموقع العام',
'cfg_page_demo' => 'يأتي القالب مع صفحة تجريبية تسمح لك باختبار التصميم مع محتويات وهمية:',
'cfg_page_demo_article' => 'مقالة تجريبية',
'cfg_bouton1_article' => 'مقالة الزر 1',
'cfg_bouton2_article' => 'مقالة الزر 2',
'cfg homepage intro' => 'مقالة الزر 2',
'cfg_home_intro_titre' => 'العنوان',
'cfg_home_intro_soustitre' => 'العنوان الفرعي',
'cfg_home_intro_texte' => 'النص',
'contact' => 'اتصل',
'cfg_homepage_intro' => 'الصفحة الرئيسية > مقدمة الكتلة',
'cfg_pagination_titre' => 'ترقيم الصفحات',
'cfg_pagination' => 'عدد العناصر المُدرجة في كل صفحة من ترقيم الصفحات',
'cfg_pagination_explication' => 'عندما يكون هناك العديد من النتائج، يستخدم الموقع نظام ترقيم الصفحات. يُرجى تحديد عدد العناصر المدرجة في كل صفحة. إذا كنت لا ترغب في ترقيم الصفحات، يُرجى التحديد -1',
'cfg_home_reassurance' => 'نص التطمين',
'cfg_home_reassurance_explication' => 'نص تقديم الموقع. في سياق متعدد اللغات، يرجى استخدام العلامات <multi> ... </multi>',
'cfg_home_reassurance_ids' => 'رابط الزر',
'cfg_home_reassurance_ids_explication' => '(اختياري) اختر الرابط الذي يؤدي الزر إليه. في سياق متعدد اللغات، اختر رابطًا واحدًا لكل لغة',
'cfg_home_reassurance_bouton_titre' => 'نص الزر',
'cfg_home_reassurance_bouton_titre_explication' => 'في سياق متعدد اللغات، يرجى استخدام العلامات <multi> ... </multi>',
'cfg_home_une' => 'الصفحة الرئيسية > مميز',
'cfg_home_une_ids' => 'المقالات المميزة',
'cfg_home_une_ids_explication' => '(اختياري) تم سرد المقالات المميزة بالعرض الكامل. في سياق متعدد اللغات، يرجى تحديد مقالات في كل لغة',
'cfg_home_recents' => 'الصفحة الرئيسية > مقالات حديثة',
'cfg_home_recents_ids' => 'مقالات حديثة (يدوي)',
'cfg_home_recents_ids_explication' => '(اختياري) تم سرد المقالات الحديثة بنصف العرض. حدد هنا المقالات لوضعها في القائمة العليا يدويًا (خارج القيود التاريخية). في سياق متعدد اللغات، يرجى تحديد مقالات في كل لغة',
'cfg_home_recents_nb_articles' => 'عدد المقالات الحديثة (تلقائي)',
'cfg_home_recents_nb_articles_explication' => 'عدد المقالات الحديثة المدرجة تلقائيًا حسب التاريخ. لعدم مدرجة أي شيء تلقائيًا، يرجى تحديد -1',
'cfg_home_aussi' => 'الصفحة الرئيسية > وأيضًا',
'cfg_home_aussi_ids' => 'وأيضًا... (يدوي)',
'cfg_home_aussi_ids_explication' => '(اختياري) حدد المقالات لوضعها في "قراءة أيضًا". يتم سردها كقائمة بسيطة. في سياق متعدد اللغات، يرجى تحديد مقالات في كل لغة',
'cfg_home_aussi_nb_articles' => 'عدد مقالات "قراءة أيضًا" (تلقائي)',
'cfg_home_aussi_nb_articles_explication' => 'عدد مقالات "قراءة أيضًا" للسرد التلقائي حسب التاريخ. لعدم سرد أي شيء تلقائيًا، يرجى التحديد -1',



// E
'et_aussi' => 'انظر أيضًا ...',
'en_savoir_plus' => 'تعرف أكثر',

// F
'forum' => 'منتدى',
'forum_derniers' => 'أحدث المواضيع على المنتدى',
'forum_acceder' => 'الوصول إلى المنتدى',
'forums_nb_post' => 'هناك أكثر من @nb@ موضوع',
'forums_nb_post_0' => 'لا توجد ردود على هذه الرسالة',
'forums_nb_post_1' => 'هناك 1 رد متاح',
'forums_nb_post_nb' => 'هناك @nb@ ردود متاحة',
'forums_post_cta' => 'اكتب رسالة جديدة',
'forum_recherche' => 'البحث في المنتدى',
'forum_champs_dernier_post' => 'آخر الرسائل',
'forum_champs_dernier_thread' => 'أحدث المواضيع',
'forum_champs_auteur' => 'المؤلف',
'forum_champs_date' => 'التاريخ',
'forum_poster_nouveau_message' => 'نشر رسالة جديدة',
'forums_lire_titre' => 'المشاركة',
'forums_lire_texte' => 'عرض والرد على الرسائل المنشورة',
'forums_lire_cta' => 'أشارك',
'forums_post_titre' => 'نشر رسالة جديدة',
'forums_post_texte' => 'بدء مناقشة',
'forums_post_reply_cta' => 'نشر رد جديد',
'forum_post_recent' => 'جديد',
'forum_recherche_annuler' => 'إلغاء هذا البحث',
'forum_recherche_titre' => 'النتائج على المنتدى',


// L
'liens' => 'روابط',
'lire_la_suite' => 'اقرأ المزيد',
'lire_la_suite_decouvrir' => 'اكتشاف',
'les_evenements' => 'الفعاليات',


// M
'menu' => 'القائمة',
'menu_lang' => 'اللغة',
'mis_a_jour' => 'تم التحديث في',



// O
'ours' => '👺 <a href="https://www.erational.org">erational</a> تم تصميم هذا القالب بواسطة',


// P
'publie_le' => 'نُشر في',
'par' => 'بواسطة',
'pagination_pages' => 'الصفحات',
'pagination_gd_total' => 'المقالات المتاحة',
'pagination_environ' => 'تقريباً',
'portfolio' => 'ألبوم الصور',
'presentation' => 'التخطيط',


// R
'resultats_out' => 'النتيجة/النتائج متوفرة',
'recherche_site' => ' :النتيجة على',
'recherche_recherche' => 'البحث',
'recherche_archive' => 'البحث',
'recherche_nomatch'  => 'آسف، <strong>لا توجد نتائج</strong> متاحة لهذا البحث! <br>قم بتعديل بحثك لتوسيع النتائج أو استخدم الرمز * كحرف مركب',
'resultats_articles' => 'البحث في المقالات',
'recherche_dans_rubrique' => 'البحث في هذا القسم',
'recherche_resultat' => 'نتائج البحث عن',
'recherche_titre' => 'البحث',
'recherche_cancel' => 'إلغاء هذا البحث',
'retour_liste' => 'العودة إلى القائمة',
'resultats' => '&nbsp;النتيجة/النتائج',
'repondre_article' => 'تعليق',


// T
'top' => 'أعلى الصفحة',
'titre_page_configurer_ginza' => 'ضبط ginza',

// V
'ginza_type_rubrique' => 'نوع القسم',
'ginza_type_rubrique_tri_date' => 'المقالات المدرجة حسب التاريخ (المقالات الأحدث أولاً)',
'ginza_type_rubrique_tri_num' => 'المقالات المدرجة حسب الرقم (10. xxx، 20. yyy، ...)',
'ginza_type_rubrique_tri_faq' => 'المقالات المدرجة كقاعدة معرفية (الأسئلة الشائعة)',
'ginza_type_rubrique_tri_evenement' => 'المقالات المدرجة كجدول زمني (قائمة الأحداث)',
'ginza_rubrique_surtitre' => 'عنوان فرعي',
'ginza_rubrique_surtitre_explication' => '(اختياري) يسمح بعرض عبارة قصيرة فوق العنوان، خاصة على الصفحة الرئيسية',
'ginza_rubrique_titre_long' => 'العنوان الطويل',
'ginza_rubrique_titre_long_explication' => '(اختياري) يسمح بعرض عنوان طويل، خاصة على الصفحة الرئيسية',


];

<?php
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function ginza_declarer_champs_extras($champs = array()) {

	// Table spip_rubriques
	$champs['spip_rubriques']['ginza_type_rubrique'] = array(
		'saisie' => 'radio',
		'options' => array(
			'nom' => 'ginza_type_rubrique',
			'label' => _T('ginza:ginza_type_rubrique'),
			'sql' => "varchar(30) NOT NULL DEFAULT ''",
			'defaut' => 'tri_date',
			'data' => array(
				'tri_date' => _T('ginza:ginza_type_rubrique_tri_date'),
 				'tri_num' => _T('ginza:ginza_type_rubrique_tri_num'),
			),
			'restrictions' => array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);

	// Table spip_articles
	$champs['spip_articles']['cacher_date'] = array(
		'saisie' => 'case',
		'options' => array(
			'nom' => 'cacher_date',
			'label_case' => _T('ginza:cacher_date'),
			'sql' => "varchar(3) NOT NULL DEFAULT ''",
			'defaut' => '',
			'restrictions'=>array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => '')),	//Tout le monde peuvent modifier
		),
	);


	return $champs;
}

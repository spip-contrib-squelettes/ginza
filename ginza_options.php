<?php
/**
 * Chargement du plugin Ginza
 *
 * @plugin     Ginza
 * @copyright  2024
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Ginza\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// tag des intertitres
$GLOBALS['debut_intertitre'] = "\n<h2 class=\"article-h2\">\n";
$GLOBALS['fin_intertitre'] = "\n</h2>\n";

// pagination : 1+5 liens max
define('_PAGINATION_NOMBRE_LIENS_MAX', 5);

// activer le surlignage de recherche
define('_SURLIGNE_RECHERCHE_REFERERS',true);

if (isset($_REQUEST['recherche'])) {
	$_GET['var_recherche'] = $_REQUEST['recherche'];
}


// fonction extra

// https://contrib.spip.net/Filtre-duree
// Définir le fuseau horaire par défaut à utiliser.
# date_default_timezone_set('UTC');
/**
 *
 * Calculer les durées écoulées entre deux dates au format Y-m-d H:i:s
 * @param date $date_debut
 * @param date|string $date_fin
 * Sans le parametre $date_fin == today
 *
 * @param string $type_affichage
 * par defaut court, sinon affichage des durées année/mois/jours/heures …
 *
 * @example dans un squelette
 	#SET{date_debut_brut_sql,2006-05-03 18:30:00}
 	#SET{date_fin_brut_sql,2022-06-23 17:29:56}

 	<h3>Durées écoulées</h3>
 	Entre son arrivée le [(#GET{date_debut_brut_sql})] et aujourd'hui<br>
 	<span style="color:red">[(#GET{date_debut_brut_sql}|duree)]</span><br>
 	Ou bien avec la date de fin [(#GET{date_fin_brut_sql})] <br>
 	<span style="color:green">[(#GET{date_debut_brut_sql}|duree{#GET{date_fin_brut_sql}})]</span>
 *
 * @return string
 * par defaut le nombre de jours
 * sinon année/mois/jours/heures
 *
**/
function duree($date_debut,$date_fin = '') {
	if(!$date_fin) {
		$date_fin = date("Y-m-d H:i:s");
	}

	$origin = new DateTimeImmutable($date_debut);
	$target = new DateTimeImmutable($date_fin);
	$interval = $origin->diff($target);

	$duree_affichage = $interval->format('%R%a'); //nb_jours

	return $duree_affichage;
}

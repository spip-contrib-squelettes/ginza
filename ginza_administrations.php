<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin ginza
 *
 * @plugin     ginza
 * @copyright  2023
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\ginza\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/meta');
include_spip('inc/cextras');
include_spip('base/ginza');


/**
 * Fonction d'installation et de mise à jour du plugin ginza.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function ginza_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	cextras_api_upgrade(ginza_declarer_champs_extras(), $maj['create']);
	cextras_api_upgrade(ginza_declarer_champs_extras(), $maj['1.0.1']);
	cextras_api_upgrade(ginza_declarer_champs_extras(), $maj['1.0.2']);
	cextras_api_upgrade(ginza_declarer_champs_extras(), $maj['1.0.3']);
	cextras_api_upgrade(ginza_declarer_champs_extras(), $maj['1.0.4']);
	cextras_api_upgrade(ginza_declarer_champs_extras(), $maj['1.0.5']);
	cextras_api_upgrade(ginza_declarer_champs_extras(), $maj['1.0.7']);

	$maj['1.6.0'] = array(
		array('ginza_desactiver_mediabox'),
	);


	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin ginza
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function ginza_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(ginza_declarer_champs_extras());
	effacer_config('ginza');
	effacer_meta('mediabox');
	effacer_meta($nom_meta_base_version);
}

/**
 * Fonction pour désactiver mediabox et éviter les conflits de js
 *
 * @return void
**/
function ginza_desactiver_mediabox() {
	effacer_meta('mediabox');
	$config['active'] = 'non';
	ecrire_meta('mediabox', serialize($config));
}

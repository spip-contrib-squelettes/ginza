/********************************
 *  GUI: interface utilisateur
 *
 ********************************/

$(document).ready(function() {

	// Lien en spip_out s'ouvre ds une nouvelle fenetre
	$('a.spip_out,a[rel*=external]').attr('target', '_blank').attr('rel', 'external noopener noreferrer');

	// Go to the top
	// http://www.jqueryscript.net/other/Minimal-Back-To-Top-Functionality-with-jQuery-CSS3.html
	$(window).scroll(function(event) {
		var scroll = $(window).scrollTop();
		if (scroll >= 800) {
			$(".go-top").addClass("show");
		} else {
			$(".go-top").removeClass("show");
		}
	});

	$('.go-top a').click(function() {
		$('html, body').animate({
			scrollTop: $("#top").offset().top
		}, 1000);
		return false;
	});

	// menu dock
	$('.button-burger-trigger').click(function() {
		$(".menu-panel").addClass('unfold');
		return false;
	});
	$('.button-burger-untrigger').click(function() {
		$(".menu-panel").removeClass('unfold');
		return false;
	});

	// barre recherche
	$('.header-search-trigger').click(function() {
		$(".header-search").addClass('unfold');
		return false;
	});
	$('.header-search-untrigger').click(function() {
		$(".header-search").removeClass('unfold');
		return false;
	});

	// bouton pills
	$('button.bouton-puce').click(function() {
		// menu plie / deplie
		var target = $(this).attr('data-target');
		if (target) {
			// reset les menus ouverts
			$(".bouton-puce--hidden").removeClass("bouton-puce--hidden");
			$(".header-menu-pills").addClass("header-menu-pills--hidden");
			// appliquer la cible
			$(this).addClass("bouton-puce--hidden");
			$("#"+target).removeClass("header-menu-pills--hidden");
		}
		// lien
		var href = $(this).attr('data-href');
		if (href) {
			window.location = href;
		}		
		return false;
	});

	// scroll doux
	$('.smooth-scroll').click(function() {
		var target = $(this).attr('href');
		var hash = "#" + target.substring(target.indexOf('#') + 1);
		$('html, body').animate({
			scrollTop: $(hash).offset().top
		}, 600);
		return false;
	});

	// fancybox >	la legende modale est le tag figcaption
	// doc.
	// https://fancyapps.com/fancybox/3/docs/#faq
	// https://codepen.io/fancyapps/pen/aaerxw


	$('[data-fancybox="images"]' ).fancybox({
		// tempo slideshow
		slideShow: {
			speed: 4000
		},

		// gestion des legendes
		caption : function( instance, item ) {
			var legende = $(this).parent().find('figcaption').html();
			if (typeof legende !== 'undefined') {
				return legende;
			}
			return "<!--  -->"; // pas de legende
		}
	});




});

// Vanilla JS here

// WOW: animation in scroll
// https://wowjs.uk/docs.html
new WOW().init();
